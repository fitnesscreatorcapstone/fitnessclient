import {Component, OnInit} from '@angular/core';
import {FitnessService} from '../services/fitness.service';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css']
})
export class CalendarComponent implements OnInit {

  userWorkouts: Observable<any>;
  userId: number;
  constructor(private fitnessServer: FitnessService) { }

  ngOnInit() {
    this.fitnessServer.getUserId(sessionStorage.getItem('AuthUsername')).subscribe(userId => {this.userId = userId; },
      () => {},
      () => {this.fitnessServer.getAllUserSelectedWorkouts(this.userId).subscribe(userWorkout => {this.userWorkouts = userWorkout; }); });
  }

}
