import { Component, OnInit } from '@angular/core';
import {FitnessService} from "../services/fitness.service";

@Component({
  selector: 'app-catalog',
  templateUrl: './catalog.component.html',
  styleUrls: ['./catalog.component.css']
})
export class CatalogComponent implements OnInit {

  fitnessDatabase: Array<any>;

  constructor(private fitnessService: FitnessService) { }

  ngOnInit() {
    this.fitnessService.getExercise().subscribe(data => { this.fitnessDatabase = data; },
      () => {}, () => {});
  }

  printMe(): void {
    console.log("hello");
    console.log(this.fitnessDatabase[0]);
  }


}
