import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FitnessService {

    // ***** Member Variables *****
    private rootUrl = 'https://fitness-server-insightful-sitatunga.apps.pcf.pcfhost.com';
    // private rootUrl = '//localhost:8080';
    // ***** Constructors *****
    constructor(private http: HttpClient) { }

    // ***** Functions *****
    public getUserId(username: string): Observable<any> {
      return this.http.get(this.rootUrl + '/user/' + username);
  }
    public getWorkout(intensity: number, type: number, option: number): Observable<any> {
      return this.http.get(this.rootUrl + '/workout/' + intensity + '/' + type + '/' + option);
    }

    public saveWorkout(workout: object) {
      return this.http.post(this.rootUrl + '/selectedUserWorkouts', workout);
    }
    public getCatalog(name: string, image: string, description: string): Observable<any> {
      return this.http.get(this.rootUrl + '/catalog/' + name + '/' + image + '/' + description);
    }

    public getExercise(): Observable<any> {
      return this.http.get(this.rootUrl + '/exercises');
    }

    public getPosts(): Observable<any> {
      return this.http.get(this.rootUrl + '/forums');
    }

    public createPost(item: object) {
      console.log(item);
      return this.http.post(this.rootUrl + '/forums', item);
    }

    public getIntensity(): Observable<any> {
      return this.http.get(this.rootUrl + '/intensities');
    }

    public getType(): Observable<any> {
      return this.http.get(this.rootUrl + '/types');
    }

    public getWorkoutOption(): Observable<any> {
      return this.http.get(this.rootUrl + '/workoutoptions');
    }

    public getAllNews(): Observable<any> {
      return this.http.get(this.rootUrl + '/news');
    }
    public getAllUserSelectedWorkouts(userId: number): Observable<any> {
      return this.http.get(this.rootUrl + '/userSelectedWorkouts/' + userId);
    }
}
