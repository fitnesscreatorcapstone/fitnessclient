import { Component, OnInit } from '@angular/core';
import {FitnessService} from '../services/fitness.service';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit {
  // ***** Member Variables *****
  newsDatabase: Array<any>;

  // ***** Constructors *****
  constructor(public fitnessService: FitnessService) {
  }

  ngOnInit() {
    this.fitnessService.getAllNews().subscribe(news => { this.newsDatabase = news; },
      () => {}, () => {});
  }
}


