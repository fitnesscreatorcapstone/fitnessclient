import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {HomepageComponent} from './homepage/homepage.component';
import {CommonModule} from '@angular/common';
import {GeneratorComponent} from './generator/generator.component';
import {CalendarComponent} from './calendar/calendar.component';
import {ForumComponent} from './forum/forum.component';
import {CatalogComponent} from './catalog/catalog.component';
import {NewsComponent} from './news/news.component';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';


const routes: Routes = [
  {path: 'home', component: HomepageComponent},
  {path: 'generator', component: GeneratorComponent},
  {path: 'calendar', component: CalendarComponent},
  {path: 'forum', component: ForumComponent},
  {path: 'catalog', component: CatalogComponent},
  {path: 'news', component: NewsComponent},
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    CommonModule],
  exports: [RouterModule]
})

export class AppRoutingModule {}
