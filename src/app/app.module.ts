import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { NavbarComponent } from './navbar/navbar.component';
import { HomepageComponent } from './homepage/homepage.component';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AppRoutingModule} from './app-routing.module';
import { GeneratorComponent } from './generator/generator.component';
import { CalendarComponent } from './calendar/calendar.component';
import { ForumComponent } from './forum/forum.component';
import { CatalogComponent } from './catalog/catalog.component';
import {MatFormFieldModule, MatSelectModule} from '@angular/material';
import { NewsComponent } from './news/news.component';
import { LoginComponent } from './login/login.component';
import {httpInterceptorProviders} from './auth/auth-interceptor';
import {RegisterComponent} from './register/register.component';
import {HashLocationStrategy, LocationStrategy} from "@angular/common";
import {MatButtonModule} from '@angular/material/button';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    NavbarComponent,
    HomepageComponent,
    GeneratorComponent,
    CalendarComponent,
    ForumComponent,
    CatalogComponent,
    NewsComponent,
    LoginComponent,
    RegisterComponent
  ],
  imports: [
    MatButtonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    BrowserModule,
    MatFormFieldModule,
    MatSelectModule,
    ReactiveFormsModule
  ],
  providers: [httpInterceptorProviders, {provide: LocationStrategy, useClass: HashLocationStrategy}],
  bootstrap: [AppComponent]
})
export class AppModule { }
