import { Component, OnInit } from '@angular/core';
import {FitnessService} from "../services/fitness.service";
import {FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-forum',
  templateUrl: './forum.component.html',
  styleUrls: ['./forum.component.css']
})
export class ForumComponent implements OnInit {

  // Member Variables *****
  forumDatabase: Array<any>;
  newPost: FormGroup;
  temp: any [];

  // Constructors *****
  constructor(private fitnessService: FitnessService) { }

  ngOnInit() {
    this.fitnessService.getPosts().subscribe(data => { this.forumDatabase = data; },
      () => {}, () => {});
    this.newPost = new FormGroup({
      post: new FormControl()
    });
  }

  // ***** Functions *****
  createPost() {
    const obj = {
      post: this.newPost.controls.post.value
    };
    console.log(obj);
    this.fitnessService.createPost(obj).subscribe(data => {this.forumDatabase = data as any[]; });
    this.newPost.reset();
  }

}
