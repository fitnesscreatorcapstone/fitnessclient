import { Component, OnInit } from '@angular/core';
import {FitnessService} from '../services/fitness.service';
import {TokenStorageService} from '../auth/token-storage.service';

@Component({
  selector: 'app-generator',
  templateUrl: './generator.component.html',
  styleUrls: ['./generator.component.css']
})
export class GeneratorComponent implements OnInit {

  intensities: Array<any>;
  workoutTypes: Array<any>;
  workoutOptions: Array<any>;
  workouts: Array<any>;
  isVisible: boolean;

  userId: any;

  first = 0;
  second = 0; // Good
  third = 0;

  constructor(private fitnessService: FitnessService, private tokenStorage: TokenStorageService) { }

  ngOnInit() {
    this.fitnessService.getType().subscribe(type => { this.workoutTypes = type; });
    this.fitnessService.getWorkoutOption().subscribe(option => { this.workoutOptions = option; });
    this.fitnessService.getIntensity().subscribe(intensity => { this.intensities = intensity; });
    // if (sessionStorage.getItem('AuthUsername') !== null) {
      this.getUserId(window.sessionStorage.getItem('AuthUsername'));
    // }
  }

  getUserId(userName: string) {
   this.fitnessService.getUserId(userName).subscribe(userNumber => {this.userId = userNumber; });
  }

  getWorkout() {
        this.fitnessService.getWorkout(this.first, this.second, this.third)
          .subscribe(workout => {this.workouts = workout; });
    }

  saveWorkout() {
    this.isVisible = this.first !== 0 && this.second !== 0 && this.third !== 0;
    if (this.isVisible) {
      this.getWorkout();
      const workout = {
        userId: {
          id: this.userId
        },
        intensityId: {
          id: this.first
        },
        typeId: {
          id: this.second
        },
        workoutId: {
          id: this.third
        }
      };
      console.log(this.userId);
      this.fitnessService.saveWorkout(workout).subscribe();
    }
  }

  saveValue(event, field ): void {
    switch (field) {
      case 'first': this.first = event.value; break;
      case 'second': this.second = event.value; break;
      case 'third': this.third = event.value; break;
    }
  }
}
